import csv
import getpass
import hashlib

menu = ("\nSelectionner une option : \n\n"
            "1. Se connecter \n"
            "2. Créer un compte")
available_options = ["1", "2"]

def get_function():
    return {"1": login, "2": register}

def login():
    print("============ Login ============")
    login = input("Login: ")
    password = getpass.getpass(prompt="Password: ")

    password_hash = hashlib.sha512(password.encode()).hexdigest()

    reader = csv.reader(open("password_storage.csv", "r"), delimiter=";")
    for row in reader:
        if row[0] == login and row[1] == password_hash:
            print("---- Connected as "+login)
            return

    print("---- No account find ----")

def register():
    print("============ Register ============")
    login = input("Login: ")

    password = getpass.getpass(prompt="Password: ")
    repassword = getpass.getpass(prompt="Confirmation Password: ")

    if password != repassword:
        print("Les mots de passes sont différents")
        return

    password_hash = hashlib.sha512(password.encode()).hexdigest()
    storage = open("password_storage.csv", "a")

    accountLine = login + ";" + password_hash + "\n"
    storage.write(accountLine)
    storage.close()

    print("---- Account created ----")

while True:
    print(menu)
    choice = input("\nChoix : ")

    if choice in available_options:
        map_function = get_function()
        map_function[choice]()