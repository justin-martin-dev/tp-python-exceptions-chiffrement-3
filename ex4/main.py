from os import path
import getpass

import pyAesCrypt

menu = ("\nSelectionner une option : \n\n"
            "1. Chiffrer un fichier \n"
            "2. Dechiffrer un fichier")
available_options = ["1", "2"]
bufferSize = 64 * 1024

def get_function():
    return {"1": encrypt, "2": decrypt}

def encrypt():
    print("============ Chiffrement ============")
    filepath = input("Chemin d'accès du fichier : ")

    encrypt_key = getpass.getpass(prompt="Clé de chiffrement: ")
    pyAesCrypt.encryptFile(filepath, "encrypted_data.txt", encrypt_key, bufferSize)

def decrypt():
    print("============ Déchiffrement ============")
    encrypt_key = getpass.getpass(prompt="Clé de chiffrement: ")
    pyAesCrypt.decryptFile("encrypted_data.txt", "decrypted_data.txt", encrypt_key, bufferSize)

while True:
    print(menu)
    choice = input("\nChoix : ")

    if choice in available_options:
        map_function = get_function()
        map_function[choice]()